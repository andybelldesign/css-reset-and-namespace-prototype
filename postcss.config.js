const enablePrefixing = true;
const cssPrefix = '.new-ds';
const replaceSelectors = ['html', 'body', cssPrefix];

module.exports = {
  plugins: [
    require('postcss-easy-import'),
    require('postcss-nested'),
    require('postcss-prefix-selector')({
      prefix: cssPrefix,
      transform(prefix, selector, prefixedSelector) {
        if (!enablePrefixing) return selector;

        switch (true) {
          case replaceSelectors.some(x => selector === x):
            return cssPrefix;
          default:
            return prefixedSelector;
        }
      }
    })
  ]
};
