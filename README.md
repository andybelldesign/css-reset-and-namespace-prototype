# Reset and Namespace prototype
A little protoype to show how we could leverage PostCSS to add a namespace for when we want to implement the header and footer on older sites. The aggressive reset should also help to reduce leaked styles.

To demonstrate it working, the context is a free Bootstrap theme that has very leaky styles. You'll see that the section that's been added `<div class="section">` resets all of these styles and comfortably rolls the ones that we define in the new CSS.

## Getting started

Clone this repo and run `npm install` at the root.

### CSS
To process CSS, run `npm run dev:css:watch` which will watch for changes.

### PostCSS
To make changes to the config, open up `postcss.config.js`. At the top you have a couple of constants that will control wether to prefix and what the prefix is.

### Running locally 
To run the site in your browser, you can either open up `dist/index.html` or run `npm run dev:serve` which will make the site available at http://localhost:3333. To skip straight to the new section that’s prefixed, go to http://localhost:3333#prefixed-section.
